package finalprojectcc;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StepDefinitions {
    ChromeDriver driver;

    @Before
    public void beforeScenario(){
        System.setProperty("webdriver.chrome.driver","C:/Users/mmart/Desktop/SDA Tester/Github/Selenium_AutomationPractice/drivers/chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        driver = new ChromeDriver(options);
        //options.setHeadless(true);
    }

    @Given("User is on main page")
    public void userIsOnMainPage() {
        driver.get("http://automationpractice.com");
    }

    @When("User clicks Sign in button on main page")
    public void userClicksSignInButtonOnMainPage() {
        WebElement signInButtonHomePage = driver.findElementByLinkText("Sign in");
        signInButtonHomePage.click();
    }

    @And("User writes {string} in Email address input field")
    public void userWritesInEmailAddressInputField(String username) {
        WebElement emailInputButton = driver.findElement(By.id("email"));
        emailInputButton.sendKeys(username);
    }

    @And("User writes {string} in Password input field with")
    public void userWritesInPasswordInputFieldWith(String password) {
        WebElement passwordInputButton = driver.findElementById("passwd");
        passwordInputButton.sendKeys(password);
    }

    @And("User clicks Sign in button")
    public void userClicksSignInButton() {
        WebElement signInButton = driver.findElement(By.id("SubmitLogin"));
        signInButton.click();
    }

    @And("User clicks Sign out")
    public void userClicksSignOut() {
        WebElement logOutButton = driver.findElement(By.linkText("Sign out"));
        logOutButton.click();
    }

    @Then("User is logged out")
    public void userIsLoggedOut() {
        WebElement authentication = driver.findElement(By.id("authentication"));
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(authentication));
    }

    @After
    public void afterScenario(){
        driver.quit();
    }

}
